package br.com.dataa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import br.com.dataa.model.Position;
import br.com.dataa.repo.PositionRepo;

@Service
public class PositionServiceImpl implements PositionService{

	@Autowired
	private PositionRepo positionRepo;

	public Position save(Position position) {
		positionRepo.save(position);
		return position;
	}


}
