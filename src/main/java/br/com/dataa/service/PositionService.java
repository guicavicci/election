package br.com.dataa.service;

import br.com.dataa.model.Position;

public interface PositionService {
	
	public Position save(Position position);

}
