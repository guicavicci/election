package br.com.dataa.service;

import java.util.List;

import br.com.dataa.model.Candidate;
import javassist.NotFoundException;

public interface CandidateService {
	public Candidate save(Candidate candidate);
	public List<Candidate> findAllOrderByPosition() throws NotFoundException;

}
