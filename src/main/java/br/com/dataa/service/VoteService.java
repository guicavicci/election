package br.com.dataa.service;

import br.com.dataa.model.Vote;

public interface VoteService {
	
	public Vote save(Vote vote);

	public Long findAmountPersonVoted();
}
