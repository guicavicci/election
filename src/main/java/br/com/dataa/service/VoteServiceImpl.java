package br.com.dataa.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import br.com.dataa.model.Candidate;
import br.com.dataa.model.Vote;
import br.com.dataa.repo.VoteRepo;

@Service
public class VoteServiceImpl implements VoteService {

	@Autowired
	private VoteRepo voteRepo;

	public Vote save(Vote vote) {
		Set<Candidate> candidates = vote.getCandidates();
		List<String> positionsName = new ArrayList<String>();
		for (Candidate candidate : candidates) {
			positionsName.add(candidate.getPosition().getName());
		}

		Boolean isDuplicity = checkDuplicity(positionsName);

		if (isDuplicity) {
			throw new DuplicateKeyException("there are two candidates with the same load");
		}

		else {
			UUID protocol = UUID.randomUUID();
			vote.setProtocol(protocol);
			voteRepo.save(vote);
			return vote;
		}

	}

	public Long findAmountPersonVoted() {
		return voteRepo.findAmountPersonVoted();
	}

	private Boolean checkDuplicity(List<String> positionsName) {
		Set<String> set = new HashSet();
		Set<String> repeated = new HashSet();
		positionsName.forEach((o) -> {
			boolean add = set.add(o);
			if(!add){
				repeated.add(o);
			}
		} 
				);

		if (repeated.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
}