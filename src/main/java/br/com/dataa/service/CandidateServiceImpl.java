package br.com.dataa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import br.com.dataa.model.Candidate;
import br.com.dataa.repo.CandidateRepo;
import javassist.NotFoundException;

@Service
public class CandidateServiceImpl implements CandidateService {

	@Autowired
	private CandidateRepo candidateRepo;

	public Candidate save(Candidate candidate) {
		candidateRepo.save(candidate);
		return candidate;
	}

public List<Candidate> findAllOrderByPosition() throws NotFoundException {
	if(candidateRepo.findAllOrderByPosition().isPresent()) {
		return candidateRepo.findAllOrderByPosition().get();
	} else {
		throw new NotFoundException("votes not exist");
	}
}

}
