package br.com.dataa.service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.dataa.model.Election;
import br.com.dataa.repo.ElectionRepo;
import javassist.NotFoundException;

@Service
public class ElectionServiceImpl implements ElectionService {

	@Autowired
	private ElectionRepo electionRepo;

	public Election save(Election election) {
		electionRepo.save(election);
		return election;
	}

	public List<Election> findByPeriod() throws NotFoundException{

		List<Election> elections = new ArrayList<Election>();
		LocalDateTime dateLocalDateTime = LocalDateTime.now();
		Date date = Date.from(dateLocalDateTime.atZone(ZoneId.systemDefault()).toInstant());
		Optional<List<Election>> op = electionRepo.findByPeriod(date);
		if(op.isPresent()) {
			elections = op.get();
			return elections;
		} else {
			throw new NotFoundException("there is no active election");
		}



	}

}
