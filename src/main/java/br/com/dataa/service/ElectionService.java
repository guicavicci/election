package br.com.dataa.service;

import java.util.List;

import br.com.dataa.model.Election;
import javassist.NotFoundException;

public interface ElectionService {
	
	public Election save(Election election);
	public List<Election> findByPeriod() throws NotFoundException;
}
