package br.com.dataa.payload;

import java.util.List;

import br.com.dataa.model.Candidate;
import lombok.Data;

@Data
public class VotePayload {
	
	private String nameElector;
	private String cpf;
	private List<Long> candidatesId;
	

}
