package br.com.dataa.payload;

import lombok.Data;

@Data
public class ElectionPayload {
	
	private String name;
	private String initialDate;
	private String finalDate;

}
