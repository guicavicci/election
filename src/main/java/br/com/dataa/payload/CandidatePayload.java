package br.com.dataa.payload;

import lombok.Data;

@Data
public class CandidatePayload {
	
	private String name;
	private Long positionId;
	private Long electionId;

}
