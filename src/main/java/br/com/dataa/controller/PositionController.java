package br.com.dataa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.dataa.model.Position;
import br.com.dataa.service.PositionService;

@Controller
@RequestMapping("/cargo/cadastro")
public class PositionController {
	
	@Autowired
	private PositionService positionService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String cadastro() {
		return "WEB-INF/position/new";
		
	}
	
	@RequestMapping(method = RequestMethod.POST)
		public String cadastro(Position position) {
		positionService.save(position);
		
		return "/WEB-INF/position/success";
		
	}

}
