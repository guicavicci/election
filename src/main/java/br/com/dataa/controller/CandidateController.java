package br.com.dataa.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.dataa.model.Candidate;
import br.com.dataa.model.Election;
import br.com.dataa.model.Position;
import br.com.dataa.payload.CandidatePayload;
import br.com.dataa.repo.ElectionRepo;
import br.com.dataa.repo.PositionRepo;
import br.com.dataa.service.CandidateService;
import br.com.dataa.service.ElectionService;
import javassist.NotFoundException;

@Controller
@RequestMapping("/candidato/cadastro")
public class CandidateController {
	
	@Autowired
	private CandidateService candidateService;
	
	@Autowired
	private PositionRepo positionRepo;
	
	@Autowired
	private ElectionService electionService;
	
	@Autowired
	private ElectionRepo electionRepo;
	
	@RequestMapping(method = RequestMethod.GET)
	public String cadastro(HttpServletRequest request) {
		
		List<Position> positions = positionRepo.findAll();
		
		request.setAttribute("positions", positions);
		
		try {
			List<Election> elections = electionService.findByPeriod();
			request.setAttribute("elections", elections);
			
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		
		return "WEB-INF/candidate/new";
		
	}
	
	@RequestMapping(method = RequestMethod.POST)
		public String cadastro(CandidatePayload candidatePayload) throws NotFoundException {
		Candidate candidate = new Candidate();
		candidate.setName(candidatePayload.getName());
			
		Optional<Position> opPosition = positionRepo.findById(candidatePayload.getPositionId());
		if(opPosition.isPresent()) {
			Position position = opPosition.get();
			candidate.setPosition(position);
		} else {
			throw new NotFoundException("position does not exist");
		}
		
		Optional<Election> opElection = electionRepo.findById(candidatePayload.getElectionId());
		if(opElection.isPresent()) {
			Election election = opElection.get();
			candidate.setElection(election);
		} else {
			throw new NotFoundException("election does not exist");
		}
		candidateService.save(candidate);
		return "/WEB-INF/candidate/success";
		
	}

}
