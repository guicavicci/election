package br.com.dataa.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.dataa.model.Candidate;
import br.com.dataa.model.Position;
import br.com.dataa.payload.CandidatePayload;
import br.com.dataa.repo.PositionRepo;
import br.com.dataa.service.CandidateService;
import br.com.dataa.service.ReportService;
import br.com.dataa.service.VoteService;
import javassist.NotFoundException;

@Controller
@RequestMapping("/resultado")
public class ReportController {

	@RequestMapping(value = "/parcial", method = RequestMethod.GET )
	public String resultPartial(HttpServletRequest request) {

		//TODO

		return "WEB-INF/report/partial";

	}

	@RequestMapping(value = "/final", method = RequestMethod.GET )
	public String resultEnd(HttpServletRequest request) {

		//TODO

		return "WEB-INF/report/end";

	}

}
