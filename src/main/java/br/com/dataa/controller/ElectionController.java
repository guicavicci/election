package br.com.dataa.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.dataa.model.Election;
import br.com.dataa.payload.ElectionPayload;
import br.com.dataa.service.ElectionService;

@Controller
@RequestMapping("eleicao/cadastro")
public class ElectionController {
	
	@Autowired
	private ElectionService electionService;
	
	@RequestMapping(method = RequestMethod.GET)
	public String cadastro() {
		return "WEB-INF/election/new";
		
	}
	
	@RequestMapping(method = RequestMethod.POST)
		public String cadastro(ElectionPayload electionPayload) {
		Election election = new Election();
		election.setName(electionPayload.getName());
		Date initialDate;
		Date finalDate;
		try {
			initialDate = dateFormatter(electionPayload.getInitialDate());
			election.setInitialDate(initialDate);
			finalDate = dateFormatter(electionPayload.getFinalDate());
			election.setFinalDate(finalDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		electionService.save(election);
		
		return "/WEB-INF/election/success";
		
	}
	
	private Date dateFormatter(String dateString) throws ParseException {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = (Date)formatter.parse(dateString);
		return date;
		
	}

}
