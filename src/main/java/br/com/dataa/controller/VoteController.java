package br.com.dataa.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.dataa.model.Candidate;
import br.com.dataa.model.Election;
import br.com.dataa.model.Vote;
import br.com.dataa.payload.VotePayload;
import br.com.dataa.repo.CandidateRepo;
import br.com.dataa.repo.ElectionRepo;
import br.com.dataa.repo.PositionRepo;
import br.com.dataa.service.CandidateService;
import br.com.dataa.service.ElectionService;
import br.com.dataa.service.VoteService;
import javassist.NotFoundException;

@Controller
@RequestMapping("/voto")
public class VoteController {
	
	@Autowired
	private VoteService voteService;
	
	@Autowired
	private CandidateService candidateService;
	
	@Autowired
	private CandidateRepo candidateRepo;
	
	@Autowired
	private PositionRepo positionRepo;
	
	@Autowired
	private ElectionService electionService;
	
	@Autowired
	private ElectionRepo electionRepo;
	
	@RequestMapping(method = RequestMethod.GET)
	public String cadastro(HttpServletRequest request) {
		
		try {
			List<Election> elections = electionService.findByPeriod();
			request.setAttribute("elections", elections);
			
		} catch (NotFoundException e) {
			e.printStackTrace();
		}
		
		/*List<Candidate> candidates = candidateRepo.findAll();
		request.setAttribute("candidates", candidates); */
		
		List<String> positions = positionRepo.findaAllDistinct();
		
		request.setAttribute("positions", positions);
		
		return "WEB-INF/vote/new";
		
		
	}
	
	@RequestMapping(method = RequestMethod.POST)
		public String cadastro(VotePayload votePayload) {
		Vote vote = new Vote();
		vote.setNameElector(votePayload.getNameElector());
		vote.setCpf(votePayload.getCpf());
		
		Set<Candidate> candidates = candidateRepo.findByMultiplesId(votePayload.getCandidatesId());
		List<Election> elections = new ArrayList<Election>();
		
		for (Candidate candidate : candidates) {
			Optional<Election> opElection = electionRepo.findById(candidate.getElection().getId());
			if(opElection.isPresent()) {
				Election election = opElection.get();
				elections.add(election);
			}
			
		}
		
		vote.setCandidates(candidates);
		vote.setElections(elections);
		Vote voteSave = voteService.save(vote);
		
		return "/WEB-INF/candidate/success";
		
	}

}
