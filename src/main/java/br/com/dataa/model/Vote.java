package br.com.dataa.model;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "VOTE")
public class Vote {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "vote_candidate",
        joinColumns = @JoinColumn(name = "vote_id", referencedColumnName = "id"),
        inverseJoinColumns = @JoinColumn(name = "candidate_id", referencedColumnName = "id"))
    private Set<Candidate> candidates;
	
	private String nameElector;
	
	private String cpf;
	
	private UUID protocol;
	
	@ManyToMany(mappedBy="electionVotes")
    private List<Election> elections;
	
	public Vote(String nameElector, Candidate... candidates) {
        this.nameElector = nameElector;
        this.candidates = Stream.of(candidates).collect(Collectors.toSet());
        this.candidates.forEach(x -> x.getVotes().add(this));
    }
	

}
