package br.com.dataa.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "ELECTION")
@Data
public class Election {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@NotNull(message = "O nome é uma informação obrigatória.")
	private String name;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@NotNull(message = "A data inicial é uma informação obrigatória.")
	private Date initialDate;
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	@NotNull(message = "A data final é uma informação obrigatória.")
	private Date finalDate;
	
	@OneToMany(
	        mappedBy = "election",
	        cascade = CascadeType.ALL,
	        orphanRemoval = true
	    )
	 private List<Candidate> candidates = new ArrayList<>();
	
	 @ManyToMany
	    @JoinTable(name="election_votes", joinColumns=
	    {@JoinColumn(name="election_id")}, inverseJoinColumns=
	      {@JoinColumn(name="vote_id")})
	    private List<Vote> electionVotes;
		
}
