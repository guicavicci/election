package br.com.dataa.repo;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.dataa.model.Candidate;

@Repository
public interface CandidateRepo extends CrudRepository<Candidate, Long>{
	
	@Query("SELECT c from Candidate c \n" + 
			"where c.votes= \n" + 
			"     (select max(cc.votes) from Candidate cc)"
			+ "GROUP BY c.position")
	Optional<List<Candidate>> findAllOrderByPosition();
		
	@Override
    List<Candidate> findAll();
	

	@Query("SELECT c from Candidate c \n" + 
			"where c.id IN :ids")
	Set<Candidate> findByMultiplesId(@Param("ids")List<Long> ids);
	


}
