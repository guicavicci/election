package br.com.dataa.repo;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.dataa.model.Candidate;
import br.com.dataa.model.Election;

@Repository
public interface ElectionRepo extends CrudRepository<Election, Long>{
	
	@Query("SELECT e FROM Election e WHERE e.initialDate <= :date AND e.finalDate >= :date")
	Optional<List<Election>> findByPeriod(@Param("date")Date date);
	
	@Query("SELECT e from Election e \n" + 
			"where e.id IN :ids")
	List<Election> findByMultiplesId(@Param("ids")List<Long> ids);
}
