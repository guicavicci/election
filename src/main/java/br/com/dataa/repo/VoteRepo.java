package br.com.dataa.repo;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dataa.model.Vote;

@Repository
public interface VoteRepo extends CrudRepository<Vote, Long>{
	
	@Query("select DISTINCT(v.cpf) from Vote v")
	Long findAmountPersonVoted();


}
