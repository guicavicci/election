package br.com.dataa.repo;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.dataa.model.Position;

@Repository
public interface PositionRepo extends CrudRepository<Position, Long>{
	
	@Override
    List<Position> findAll();
	
	
	@Query("SELECT DISTINCT(c.name) FROM Position c")
	List<String> findaAllDistinct();
}
