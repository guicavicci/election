
# Election

## Definition


  Project for company Data-a using Java 11, with the following libraries
-Lombok
-Thymeleaf
-Postgresql

## Quick Start

Create a property file with service configurations:

```properties
## Spring DATASOURCE (DataSourceAutoConfiguration & DataSourceProperties)
spring.datasource.url=jdbc:postgresql://localhost:5432/dataa
spring.datasource.username=postgres
spring.datasource.password=

# The SQL dialect makes Hibernate generate better SQL for the chosen database
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQLDialect

# Hibernate ddl auto (create, create-drop, validate, update)
spring.jpa.hibernate.ddl-auto=update
spring.thymeleaf.cache=false

server.port=8080
```
**Start project**
To start the project you will need to install the following dependencies:
[https://docs.docker.com/v17.09/engine/installation/](https://docs.docker.com/v17.09/engine/installation/)
[https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

Once installed, create a file named docker-compose.yml and paste the following contents into the file:

```yml
version: '3'
  services:
	postgres:
	image: postgres
	ports:
		- "5432:5432"
	environment:
		- POSTGRES_DB=dataa
	volumes:
		- /pgdata:/var/lib/postgresql/data
	networks:
		- postgres-compose-network
	pgadmin-compose:
		image: dpage/pgadmin4
		environment:
			PGADMIN_DEFAULT_EMAIL: "teste@teste.com"
			PGADMIN_DEFAULT_PASSWORD: "mudar@123"
		ports:
			- "8090:80"
		depends_on:
			- postgres
		networks:
			- postgres-compose-network
 
	election:
		image: guicavicci/election
		environment:
			HOST_DB: postgres
		ports:
			- 8080:8080
		depends_on:
			- postgres
		networks:
			- postgres-compose-network
networks:
	postgres-compose-network:
	driver: bridge
```
  
Run command:
**docker-compose up**

**Endpoints**

Homepage: http://localhost:8080/

Register position: http://localhost:8080/cargo/cadastro

Register election: http://localhost:8080/eleicao/cadastro

Register candidate: http://localhost:8080/candidato/cadastro

Register vote: http://localhost:8080/voto
